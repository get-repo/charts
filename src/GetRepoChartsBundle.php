<?php

namespace GetRepo\Charts;

use GetRepo\Charts\DependencyInjection\GetRepoChartsExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class GetRepoChartsBundle extends Bundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        if (null === $this->extension) {
            $this->extension = $this->createContainerExtension();
        }

        return $this->extension;
    }

    protected function getContainerExtensionClass(): string
    {
        return GetRepoChartsExtension::class;
    }
}
