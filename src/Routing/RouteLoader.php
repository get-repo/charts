<?php

namespace GetRepo\Charts\Routing;

use GetRepo\Charts\DependencyInjection\GetRepoChartsExtension;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class RouteLoader extends Loader
{
    /* @var array */
    private array $config;

    private bool $isLoaded = false;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function load($resource, string $type = null): RouteCollection
    {
        if (true === $this->isLoaded) {
            throw new \RuntimeException(sprintf('Do not add the "%s" loader twice', GetRepoChartsExtension::ALIAS));
        }

        $routes = new RouteCollection();
        $routes->add(
            GetRepoChartsExtension::ALIAS . '_chart_data',
            new Route(
                $this->config['routing']['chart_data'],
                ['_controller' => 'GetRepo\Charts\Controller\ChartController::dataAction'],
                ['name' => '^[a-z0-9_]+$'],
                [],
                '',
                [],
                $this->config['routing']['chart_data_method'],
                ''
            )
        );

        $this->isLoaded = true;

        return $routes;
    }

    public function supports($resource, string $type = null): bool
    {
        return GetRepoChartsExtension::ALIAS === $type;
    }
}
