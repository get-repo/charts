<?php

namespace GetRepo\Charts\Controller;

use GetRepo\Charts\DependencyInjection\GetRepoChartsExtension;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ChartController
{
    use ContainerAwareTrait;

    public const DEFAULT_TIME = '-5 minutes';

    public function chartAction(Request $request, string $name): Response
    {
        $definition = $this->getConfigChart($name);
        $config = $this->getConfig();

        switch ($definition['type']) {
            case 'table':
            case 'number':
                $template = $definition['type'];
                break;

            default:
                $template = 'chart';
                break;
        }

        return new Response(
            $this->container
                ->get('twig')
                ->render(
                    "@GetRepoCharts/{$template}.html.twig",
                    [
                        'id' => uniqid(),
                        'name' => $name,
                        'chart' => $definition,
                        'http_method' => $config['routing']['chart_data_method'],
                        'alias' => GetRepoChartsExtension::ALIAS,
                    ]
                )
        );
    }

    public function dashboardAction(Request $request, string $name): Response
    {
        $config = $this->getConfig();

        return new Response(
            $this->container
                ->get('twig')
                ->render(
                    '@GetRepoCharts/dashboard.html.twig',
                    [
                        'controller' => __CLASS__,
                        'dashboard' => $this->getConfigDashboard($name),
                        'available_charts' => array_keys($config['charts'] ?? []),
                    ]
                )
        );
    }

    public function dataAction(Request $request, string $name): JsonResponse
    {
        $definition = $this->getConfigChart($name);

        $service = $definition['data_service'] ?? false;
        if (class_exists($service)) {
            $service = new $service();
        } elseif ($this->container->has($service)) {
            $service = $this->container->get($service);
        } else {
            throw new \Exception("Chart service '{$service}' was not found");
        }

        $timeFilter = $request->query->get('t', self::DEFAULT_TIME);

        return new JsonResponse($service->getData($timeFilter));
    }

    private function getConfigChart(string $name): array
    {
        $config = $this->getConfig();
        if (!isset($config['charts'][$name])) {
            throw new \Exception("Chart '{$name}' was not found");
        }

        return $config['charts'][$name];
    }

    private function getConfigDashboard(string $name): array
    {
        $config = $this->getConfig();
        if (!isset($config['dashboards'][$name])) {
            throw new \Exception("Dashboard '{$name}' was not found");
        }

        return $config['dashboards'][$name];
    }

    /**
     * @psalm-suppress UndefinedDocblockClass
     */
    private function getConfig(): array
    {
        return (array) $this->container->getParameter('getrepo_charts.config');
    }
}
