<?php

namespace GetRepo\Charts\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class GetRepoChartsExtension extends Extension implements PrependExtensionInterface
{
    public const ALIAS = 'getrepo_charts';

    public function getAlias(): string
    {
        return self::ALIAS;
    }

    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter(self::ALIAS . '.config', $config);
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(realpath(__DIR__ . '/../Resources/config'))
        );
        $loader->load('services.yml');
    }

    public function prepend(ContainerBuilder $container): void
    {
        $container->prependExtensionConfig('twig', [
            'paths' => [dirname(__DIR__) . '/Resources/templates' => 'GetRepoCharts'],
        ]);
    }
}
