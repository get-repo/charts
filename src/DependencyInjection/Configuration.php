<?php

namespace GetRepo\Charts\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    private array $charts = [];

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(GetRepoChartsExtension::ALIAS);
        $rootNode = $treeBuilder->getRootNode();

        /* @var \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition $rootNode */
        $rootNode
            ->children()
                ->arrayNode('routing')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('chart')
                            ->defaultNull()
                        ->end()
                        ->scalarNode('chart_data')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->defaultValue('/chart/data/{name}')
                            ->validate()
                                ->ifTrue(function ($path) {
                                    return false === strpos($path, '{name}');
                                })
                                ->thenInvalid('Route "chart_data" path must contain parameter {name}')
                            ->end()
                        ->end()
                        ->enumNode('chart_data_method')
                            ->values(['GET', 'POST'])
                            ->defaultValue('POST')
                        ->end()
                        ->scalarNode('default_chart_data_condition')
                            ->defaultValue('request.isXmlHttpRequest()')
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('charts')
                    ->useAttributeAsKey('name')
                    ->arrayPrototype()
                        ->children()
                            ->enumNode('type')
                                ->isRequired()
                                ->cannotBeEmpty()
                                // from https://www.chartjs.org/docs/latest/charts/
                                ->values([
                                    'line',
                                    'bar',
                                    'radar',
                                    'doughnut',
                                    'pie',
                                    'polar area',
                                    'bubble',
                                    'scatter',
                                    'number', // custom
                                    'table', // custom
                                ])
                            ->end()
                            ->scalarNode('data_service')
                                ->isRequired()
                                ->cannotBeEmpty()
                            ->end()
                            ->integerNode('refresh_interval')
                                ->defaultNull()
                            ->end()
                            ->variableNode('options')
                                ->defaultValue([])
                            ->end()
                        ->end()
                        ->validate()
                            ->always()
                            ->then(function ($chart) {
                                $chart['options']['type'] = $chart['type'];

                                return $chart;
                            })
                        ->end()
                    ->end()
                    ->validate()
                        ->ifTrue(function ($charts) {
                            $this->charts = array_keys($charts);
                            foreach ($this->charts as $name) {
                                if (!preg_match('/^[a-z0-9_]+$/', $name)) {
                                    return true;
                                }
                            }

                            return false;
                        })
                        ->thenInvalid('Invalid chart name. Only lowercase letter, number or underscore for %s')
                    ->end()
                ->end()
                ->arrayNode('dashboards')
                    ->useAttributeAsKey('name')
                    ->arrayPrototype()
                        ->addDefaultsIfNotSet()
                        ->children()
                            ->arrayNode('charts')
                                ->useAttributeAsKey('name')
                                ->arrayPrototype()
                                    ->children()
                                        ->scalarNode('label')->end()
                                        ->integerNode('size')
                                            ->defaultValue(4)
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
