<?php

namespace Test;

use Symfony\Component\Panther\Client as PantherClient;
use Symfony\Component\Panther\PantherTestCase;

class ChartControllerTest extends PantherTestCase
{
    private static PantherClient $client;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$client = static::createPantherClient([
            'webServerDir' => __DIR__,
            'router' => __DIR__ . '/Kernel.php',
        ]);
    }

    public static function tearDownAfterClass(): void
    {
        self::$client->quit();
    }

    public function testDashboardFail(): void
    {
        $crawler = self::$client->request('GET', '/dashboard/whatever');
        $text = $crawler->getText();
        $this->assertStringContainsString("Dashboard 'whatever' was not found", $text);
    }

    public function testDashboardSuccess(): void
    {
        self::$client->request('GET', '/dashboard/dashboard_1');
        $crawler = self::$client->waitFor('.chartjs-size-monitor', 5);

        $text = $crawler->getText();
        $this->assertStringContainsString('Pie', $text);
        $this->assertStringContainsString('Stepped chart', $text);
        $this->assertStringContainsString('Count', $text);
        $this->assertStringContainsString('Table', $text);
    }

    public function testChartFail(): void
    {
        $crawler = self::$client->request('GET', '/chart/whatever');
        $text = $crawler->getText();
        $this->assertStringContainsString("Chart 'whatever' was not found", $text);
    }

    public function dataChartSuccess(): array
    {
        return [
            'pie' => [
                'pie',
                [
                    'document.addEventListener' => true,
                    '"chart_pie_' => true,
                    '/test/chart/data/pie' => true,
                    'setInterval(refresh, 5000)' => true,
                ],
            ],
            'stepped' => [
                'stepped',
                [
                    'document.addEventListener' => true,
                    '"chart_stepped_' => true,
                    '/test/chart/data/stepped' => true,
                    'setInterval(refresh,' => false,
                ],
            ],
            'number' => [
                'number',
                [
                    'document.addEventListener' => true,
                    '"chart_number_' => true,
                    '/test/chart/data/number' => true,
                    'setInterval(refresh,' => false,
                ],
            ],
            'table' => [
                'table',
                [
                    'document.addEventListener' => true,
                    '"chart_table_' => true,
                    '/test/chart/data/table' => true,
                    'setInterval(refresh,' => false,
                ],
            ],
        ];
    }

    /**
     * @dataProvider dataChartSuccess
     */
    public function testChartSuccess(string $name, array $contains): void
    {
        $crawler = self::$client->request('GET', "/chart/{$name}");
        $html = $crawler->html();
        foreach ($contains as $string => $expected) {
            if ($expected) {
                $this->assertStringContainsString($string, $html);
            } else {
                $this->assertStringNotContainsString($string, $html);
            }
        }
    }

    public function dataChartDataSuccess(): array
    {
        return [
            'pie' => [
                'pie',
                [
                    '"labels":["a","b","c","d"],' => true,
                ],
            ],
            'stepped' => [
                'stepped',
                [
                    '"labels":[1,2,3,4,5],' => true,
                ],
            ],
            'table' => [
                'table',
                [
                    '{"columns":{"Col' => true,
                    ',"data":[' => true,
                ],
            ],
        ];
    }

    /**
     * @dataProvider dataChartDataSuccess
     */
    public function testChartDataSuccess(string $name, array $contains): void
    {
        $crawler = self::$client->request('GET', "/test/chart/data/{$name}");
        $html = $crawler->html();
        foreach ($contains as $string => $expected) {
            if ($expected) {
                $this->assertStringContainsString($string, $html);
            } else {
                $this->assertStringNotContainsString($string, $html);
            }
        }
    }
}
