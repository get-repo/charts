<?php

namespace Test\DataService;

class SteppedDataService
{
    public function getData(): array
    {
        $datasets = [
            'labels' => [1, 2, 3, 4, 5],
            'datasets' => [
                0 => [
                    'label' => 'Stepped',
                    'backgroundColor' => '#55beb3',
                    'steppedLine' => true,
                    'fill' => true,
                    'data' => [
                        rand(1, 20),
                        rand(20, 40),
                        rand(40, 60),
                        rand(60, 80),
                        rand(80, 100),
                    ],
                ],
            ],
        ];

        return $datasets;
    }
}
