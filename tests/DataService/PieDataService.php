<?php

namespace Test\DataService;

class PieDataService
{
    public function getData(): array
    {
        $datasets = [
            'labels' => ['a', 'b', 'c', 'd'],
            'datasets' => [
                0 => [
                    'label' => 'Dataset 1',
                    'backgroundColor' => ['#55beb3', '#be5560', '#ddff87', '#87ddff'],
                    'data' => [
                        rand(1, 4),
                        rand(1, 4),
                        rand(1, 4),
                        rand(1, 4),
                    ],
                ],
            ],
        ];

        return $datasets;
    }
}
