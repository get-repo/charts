<?php

namespace Test\DataService;

class CountDataService
{
    public function getData(): int
    {
        return rand(1, 50);
    }
}
