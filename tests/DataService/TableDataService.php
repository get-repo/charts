<?php

namespace Test\DataService;

class TableDataService
{
    public function getData(): array
    {
        return [
            'columns' => [
                'Col 1' => ['width' => '10%'],
                'Col 2' => ['width' => '20%'],
                'Col 3' => [],
            ],
            'data' => [
                ['blabla', 1, '<strong style="border-bottom: 2px solid red;">test HTML</strong>'],
                ['blabla2', 2, 'column 2'],
            ],
        ];
    }
}
