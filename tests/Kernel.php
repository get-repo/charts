<?php

namespace Test;

use GetRepo\Charts\Controller\ChartController;
use GetRepo\Charts\DependencyInjection\GetRepoChartsExtension;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\RouteCollectionBuilder;

// phpcs:disable
require __DIR__.'/../vendor/autoload.php';
// phpcs:enable

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public function registerBundles(): array
    {
        return [
            new \Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new \Symfony\Bundle\TwigBundle\TwigBundle(),
            new \GetRepo\Charts\GetRepoChartsBundle(),
        ];
    }

    protected function configureRoutes(RouteCollectionBuilder $routes): void
    {
        $routes->import('.', '/', GetRepoChartsExtension::ALIAS);
        $controller = ChartController::class;
        $routes->add('/', "{$controller}::chartAction", 'chart');
        $routes->add('/dashboard/{name}', "{$controller}::dashboardAction", 'dashboard');
        $routes->add('/chart/{name}', "{$controller}::chartAction", 'chart');
    }

    protected function configureContainer(ContainerBuilder $builder, LoaderInterface $loader): void
    {
        $builder->loadFromExtension('framework', [
            'secret' => 'TEST',
            'test' => true,
        ]);
        $builder->loadFromExtension('getrepo_charts', [
            'routing' => [
                'chart_data' => '/test/chart/data/{name}',
                'chart_data_method' => 'GET',
            ],
            'charts' => [
                'pie' => [
                    'type' => 'pie',
                    'data_service' => \Test\DataService\PieDataService::class,
                    'refresh_interval' => 5,
                ],
                'stepped' => [
                    'type' => 'line',
                    'data_service' => \Test\DataService\SteppedDataService::class,
                ],
                'number' => [
                    'type' => 'number',
                    'data_service' => \Test\DataService\CountDataService::class,
                ],
                'table' => [
                    'type' => 'table',
                    'data_service' => \Test\DataService\TableDataService::class,
                ],
            ],
            'dashboards' => [
                'dashboard_1' => [
                    'charts' => [
                        'pie' => null,
                        'stepped' => [
                            'label' => 'Stepped chart',
                        ],
                        'number' => [
                            'label' => 'Count!!',
                        ],
                        'table' => null,
                    ],
                ],
            ],
        ]);
    }
}

// phpcs:disable
if ('cli' !== php_sapi_name()) {
    define('_CLI', 0); // debug
    $kernel = new Kernel('test', true);
    $request = Request::createFromGlobals();
    $response = $kernel->handle($request);
    $response->send();
    $kernel->terminate($request, $response);
}
